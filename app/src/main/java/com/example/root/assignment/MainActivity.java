package com.example.root.assignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void tofirstpicture(View view){
        Intent intent=new Intent(this,Firstpicture.class);
        startActivity(intent);
    }
    public void tosecondpicture(View view){
        Intent intent=new Intent(this,Secondpicture.class);
        startActivity(intent);
    }
    public void tothirdpicture(View view){
        Intent intent=new Intent(this,Thirdpicture.class);
        startActivity(intent);
    }
}
